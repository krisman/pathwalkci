all: pathwalkci pathwalkci-vlv
CFLAGS=-g3 -O3

_WRAP := -Xlinker --wrap=
REALPATH =

PATHWRAP = $(_WRAP)fopen $(_WRAP)freopen $(_WRAP)open $(_WRAP)creat $(_WRAP)access $(_WRAP)__xstat \
	$(_WRAP)stat $(_WRAP)lstat $(_WRAP)fopen64 $(_WRAP)open64 $(_WRAP)opendir $(_WRAP)__lxstat \
	$(_WRAP)chmod $(_WRAP)chown $(_WRAP)lchown  $(_WRAP)symlink $(_WRAP)link $(_WRAP)__lxstat64 \
	$(_WRAP)mknod $(_WRAP)utimes $(_WRAP)unlink $(_WRAP)rename $(_WRAP)utime $(_WRAP)__xstat64 \
	$(_WRAP)mount $(_WRAP)mkfifo $(_WRAP)mkdir  $(_WRAP)rmdir $(_WRAP)scandir $(_WRAP)scandir64 \
	$(_WRAP)statvfs $(_WRAP)statvfs64 $(REALPATH) $(_WRAP)dlopen $(_WRAP)dlmopen $(REALPATH)

# static build to run from my tiny initramfs
pathwalkci:  pathwalkci.c
	$(CC) $(CFLAGS) -o $@ $<
	$(CC) $(CFLAGS) -o $@-static $< -static -g3

pathwalkci-vlv: pathwalkci.c
	gcc $(CFLAGS) -c $<
	g++ /home/krisman/work/valve-pathmatch/pathmatch.o $(PATHWRAP) pathwalkci.o -ldl -o $@

clean:
	rm -f pathwalkci *.o pathwalkci-static pathwalkci-vlv
