/*
 * Copyright 2017 Collabora.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <err.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include <ctype.h>

struct inode_vec {
	ino_t ino;
	char *target;
};

int _get_next_name(char *last) {

	if (*last == '\0')
		return 1;

	if (_get_next_name (last+1)) {
		if (*last == 'Z') {
			*last = 'A';
			return 1;
		}
		/* else if(*last == 'Z')
		 *	last = 'a';*/
		else
			(*last) ++;
	}
	return 0;

}

char *get_next_name(char *last)
{
	char *new;
	if (!last)
		return strdup("AAAAA");

	new = strdup(last);
	_get_next_name(new);
	return new;
}

int create_files(char *root, int n, struct inode_vec *inode_vec)
{
	char *next_name = NULL;
	char buf[BUFSIZ];
	int fd;
	struct stat st;
	int r;
	int i;

	for (i = 0; i < n; i++) {
		next_name = get_next_name(next_name);
		sprintf(buf, "%s/%s", root, next_name);

		fd = creat(buf, S_IRUSR|S_IWUSR);
		if (fd < 0)
			err (1, "creat failed");

		r = fstat(fd, &st);
		if (r < 0)
			err(1, "fstat failed");
		inode_vec[i].target = next_name;
		inode_vec[i].ino = st.st_ino;

		close(fd);
	}

	printf("created %d files\n", i);
}

/* we do it in a separate step to not affect measurements*/
int modify_target(int n, struct inode_vec *inode_vec)
{
	int i;
	int j;
	char* s;
	int coin = 0;

	for (i=0; i < n; i++) {
		s = inode_vec[i].target;
		for (j = 0; s[j] != '\0'; j++)
			s[j] = ((coin++)%2) ? ((char) tolower((unsigned char) s[j])):s[j];
	}
}

int search_files(char *root, int n, struct inode_vec *inode_vec)
{
	int i;
	struct stat st;
	int r;
	char buf[BUFSIZ];
	struct timeval t1, t2, elapsed, sum;
	double usecs;

	gettimeofday(&t1, NULL);

	for (i =0 ; i< n; i++) {
		sprintf(buf, "%s/%s", root, inode_vec[i].target);

		r = stat (buf, &st);
		if (r) {
			err(1, "fstat failed. cant find %s", buf);
		}

		if(st.st_ino != inode_vec[i].ino) {
			printf("%d != %d\n", st.st_ino, inode_vec[i].ino);
			assert(0);
		}
	}
	gettimeofday(&t2, NULL);
	timersub(&t2, &t1, &elapsed);

	printf("found %d files. TIME: %d.%.3d\n", i, elapsed.tv_sec, elapsed.tv_usec/1000);
}


int main(int argc, char** argv)
{
	int n = 100000;
	struct inode_vec *inode_vec;
	int ci =0;

	if (argc > 2)
		n = atoi(argv[2]);
	if (argc > 3)
		ci = 1;

	inode_vec = malloc(sizeof(struct inode_vec) * n);
	if (!inode_vec)
		err(1, "failed malloc\n");

	create_files(argv[1], n, inode_vec);
	if (ci)
		modify_target( n, inode_vec);
	search_files(argv[1], n, inode_vec);
	return 0;
}
